**Aplicación test Ruleta**

- Aplicación creada con Java EE, con uso de JPA para persistencia de datos.
- Base de datos utilizada: MySQL
- Ejemplos de servicios exportados en PostMan: https://www.getpostman.com/collections/c12f01b8627e8ab0b2c1

##Metodos:

1. createRoulette (POST)

http://ip/roulette/ws/createRoulette 

2. openRoulette (POST)

http://ip/roulette/ws/openRoulette/{idRoulette}
http://localhost:7001/roulette/ws/openRoulette/1

3. createBet (POST)

http://ip/roulette/ws/betOnRoulette

Header:
user = valor

Ejemplo JSON:
{
    "idRoulette": 1,
    "rouletteOption": "COLOR",
    "valueBet": 10000,
    "rouletteNumber": 1,
    "rouletteColor": "NEGRO"
}

4. closeRoulette (POST)

http://ip/roulette/ws/closeRoulette/{idRoulette}
http://localhost:7001/roulette/ws/closeRoulette/1

5. getRoulettesList (GET)

http://ip/roulette/ws/getRoulettesList


##Scripts de BD:

create table roulettes (id_roulette int AUTO_INCREMENT,
				status int, PRIMARY KEY (id_roulette));
				
CREATE TABLE bets_roulette (id_bet INT AUTO_INCREMENT,
				id_roulette INT,
                roulette_option VARCHAR(20),
                value_bet VARCHAR(20),
                user_bet VARCHAR(20),
				status TINYINT, 
                PRIMARY KEY (id_bet),
                FOREIGN KEY (id_roulette) REFERENCES roulettes(id_roulette)); 