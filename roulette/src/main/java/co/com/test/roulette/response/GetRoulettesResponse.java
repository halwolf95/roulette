package co.com.test.roulette.response;

import co.com.test.roulette.dto.Roulette;
import java.util.List;

/**
 *
 * @author Jesus Eduardo Martinez Guerra
 */
public class GetRoulettesResponse extends RouletteResponse {

    private List<Roulette> rouletteList;

    public List<Roulette> getRouletteList() {
        return rouletteList;
    }

    public void setRouletteList(List<Roulette> rouletteList) {
        this.rouletteList = rouletteList;
    }

}
