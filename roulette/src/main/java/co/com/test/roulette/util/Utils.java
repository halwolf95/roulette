package co.com.test.roulette.util;

/**
 *
 * @author Jesus Eduardo Martinez Guerra
 */
public class Utils {

    public static Integer convertStringToInt(String value) {
        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException ex) {
            throw new NumberFormatException(String
              .format(Constants.ERROR_CONVERT_VALUE_TO_INT, value));
        }
    }

    public static boolean isNumber(Object value) {
        try {
            Integer.parseInt(String.valueOf(value));

            return true;
        } catch (NumberFormatException ex) {

            return false;
        }
    }
}
