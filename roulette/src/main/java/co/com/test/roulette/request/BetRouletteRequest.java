/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.test.roulette.request;

/**
 *
 * @author martinezjed
 */
public class BetRouletteRequest {

    private int idRoulette;
    private String rouletteOption;
    private String rouletteColor;
    private int rouletteNumber;
    private int valueBet;

    public int getIdRoulette() {
        return idRoulette;
    }

    public void setIdRoulette(int idRoulette) {
        this.idRoulette = idRoulette;
    }

    public String getRouletteOption() {
        return rouletteOption;
    }

    public void setRouletteOption(String rouletteOption) {
        this.rouletteOption = rouletteOption;
    }

    public String getRouletteColor() {
        return rouletteColor;
    }

    public void setRouletteColor(String rouletteColor) {
        this.rouletteColor = rouletteColor;
    }

    public int getRouletteNumber() {
        return rouletteNumber;
    }

    public void setRouletteNumber(int rouletteNumber) {
        this.rouletteNumber = rouletteNumber;
    }

    public int getValueBet() {
        return valueBet;
    }

    public void setValueBet(int valueBet) {
        this.valueBet = valueBet;
    }

}
