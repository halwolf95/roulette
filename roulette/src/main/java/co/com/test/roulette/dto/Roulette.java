package co.com.test.roulette.dto;

/**
 *
 * @author Jesus Eduardo Martinez Guerra
 */
public class Roulette {

    private int Roulette;
    private String status;

    public int getRoulette() {
        return Roulette;
    }

    public void setRoulette(int Roulette) {
        this.Roulette = Roulette;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
