package co.com.test.roulette.util;

/**
 * @author Jesus Eduardo Martinez Guerra
 */
public class Constants {

    public static final String ERROR = "ERROR";
    public static final String SUCCESS = "EXITO";
    public static final String DENY = "DENEGADO";
    public static final String ERROR_MESSAGE = "Validar log de aplicación";
    public static final String SUCCESS_MESSAGE_CREATE = "La ruleta %s ha "
      + "sido creada";
    public static final String ERROR_CONVERT_VALUE_TO_INT
      = "No se puede convertir valor {%s} a número";
    public static final String ERROR_QUERY_WITHOUT_RESULTS
      = "No hay resultados de la consulta realizada";
    public static final int BET_MIN_VALUE = 0;
    public static final int BET_MAX_VALUE = 10000;
    public static final String ROULETTE_OPTION_COLOR = "COLOR";
    public static final String ROULETTE_OPTION_NUMBER = "NUMERO";
    public static final int ROULETTE_MIN_NUMBER = 0;
    public static final int ROULETTE_MAX_NUMBER = 36;
    public static final String ROULETTE_RED_COLOR = "ROJO";
    public static final String ROULETTE_BLACK_COLOR = "NEGRO";
    public static final String ERROR_BET = "Apuesta no permitida";
}
