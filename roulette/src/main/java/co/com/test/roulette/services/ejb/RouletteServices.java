package co.com.test.roulette.services.ejb;

import co.com.test.roulette.dto.LastBet;
import co.com.test.roulette.dto.Roulette;
import co.com.test.roulette.entity.BetsRoulette;
import co.com.test.roulette.entity.Roulettes;
import co.com.test.roulette.enums.EEstado;
import co.com.test.roulette.request.BetRouletteRequest;
import co.com.test.roulette.response.CloseRouletteResponse;
import co.com.test.roulette.response.GetRoulettesResponse;
import co.com.test.roulette.response.RouletteResponse;
import co.com.test.roulette.services.dao.RouletteDao;
import co.com.test.roulette.util.Constants;
import co.com.test.roulette.util.Utils;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 * @author Jesus Eduardo Martinez Guerra
 */
@Stateless
public class RouletteServices {

    private final static Logger LOGGER = Logger
      .getLogger(RouletteServices.class.getName());

    @EJB
    RouletteDao rouletteDao;

    public RouletteResponse createRoulette() {
        RouletteResponse response = new RouletteResponse();
        try {
            Integer idRoulette = rouletteDao.createRoulette();
            response.setStatus(Constants.SUCCESS);
            response.setMessage(String.format(Constants.SUCCESS_MESSAGE_CREATE,
              idRoulette));
        } catch (Exception ex) {
            response.setStatus(Constants.ERROR);
            LOGGER.severe(ex.getMessage());
        }

        return response;
    }

    public RouletteResponse openRoulette(String value) {
        RouletteResponse response = new RouletteResponse();
        try {
            int idRoulette = Utils.convertStringToInt(value);
            Roulettes roulette = rouletteDao.findRoulette(idRoulette);
            rouletteDao.openRoulette(roulette);
            response.setStatus(Constants.SUCCESS);
        } catch (Exception ex) {
            response.setStatus(Constants.DENY);
            LOGGER.severe(ex.getMessage());
        }

        return response;
    }

    public RouletteResponse betOnRoulette(String user,
      BetRouletteRequest request) {
        RouletteResponse response = new RouletteResponse();
        try {
            Roulettes roulette = findRouletteByIdAndStatud(request
              .getIdRoulette());
            if (validateRequest(request) && existsRoulette(roulette)) {
                rouletteDao
                  .createBetRoulette(
                    createBetsRouletteObject(user, request, roulette));
            } else {
                throw new Exception(Constants.ERROR_BET);
            }
            response.setStatus(Constants.SUCCESS);

        } catch (Exception ex) {
            response.setStatus(Constants.ERROR);
            LOGGER.severe(ex.getMessage());
        }

        return response;
    }

    private boolean validateRouletteOption(BetRouletteRequest request) {
        try {
            String validateString = request.getRouletteOption().toUpperCase();
            switch (validateString) {
                case Constants.ROULETTE_OPTION_COLOR:

                    return validateBetOptionColor(request
                      .getRouletteColor());
                case Constants.ROULETTE_OPTION_NUMBER:

                    return validateBetOptionNumber(request
                      .getRouletteNumber());
                default:
                    return false;
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    private boolean validateBetOptionColor(String value) {
        try {

            return value.equals(Constants.ROULETTE_BLACK_COLOR)
              || value.equals(Constants.ROULETTE_RED_COLOR);
        } catch (Exception ex) {
            throw ex;
        }
    }

    private boolean validateBetOptionNumber(int value) {
        try {

            return value >= Constants.ROULETTE_MIN_NUMBER
              && value <= Constants.ROULETTE_MAX_NUMBER;
        } catch (Exception ex) {
            throw ex;
        }
    }

    private boolean validateBetValue(int value) {
        try {
            return value >= Constants.BET_MIN_VALUE
              && value <= Constants.BET_MAX_VALUE;
        } catch (Exception ex) {
            throw ex;
        }
    }

    private Roulettes findRouletteByIdAndStatud(int idRoulette)
      throws Exception {
        try {
            return rouletteDao.findRouletteByIdAndStatud(idRoulette);
        } catch (Exception ex) {
            throw ex;
        }
    }

    private boolean existsRoulette(Roulettes roulette) throws Exception {
        try {
            return roulette != null;
        } catch (Exception ex) {
            throw ex;
        }
    }

    private boolean validateRequest(BetRouletteRequest request)
      throws Exception {
        try {

            return validateBetValue(request.getValueBet())
              && validateRouletteOption(request);
        } catch (Exception ex) {
            throw ex;
        }
    }

    private BetsRoulette createBetsRouletteObject(String user,
      BetRouletteRequest request, Roulettes roulette) {
        try {
            BetsRoulette result = new BetsRoulette();
            result.setIdRoulette(roulette.getIdRoulette());
            result.setRouletteOption(getRouletteOption(request));
            result.setValueBet(request.getValueBet());
            result.setUser(user);
            result.setStatus(EEstado.ABIERTO.value());

            return result;
        } catch (Exception ex) {
            throw ex;
        }
    }

    private String getRouletteOption(BetRouletteRequest request) {
        try {
            String validateString = request.getRouletteOption().toUpperCase();
            switch (validateString) {
                case Constants.ROULETTE_OPTION_COLOR:

                    return request.getRouletteColor();
                case Constants.ROULETTE_OPTION_NUMBER:

                    return String.valueOf(request.getRouletteNumber());
                default:
                    return "";
            }
        } catch (Exception ex) {
            throw ex;

        }
    }

    public CloseRouletteResponse closeRoulette(String value) {
        CloseRouletteResponse response = new CloseRouletteResponse();
        try {

            int idRoulette = Utils.convertStringToInt(value);
            Roulettes roulette = findRouletteByIdAndStatud(idRoulette);
            if (existsRoulette(roulette)) {
                List<BetsRoulette> lastBets = rouletteDao
                  .findBetsByRouletteAndStatus(idRoulette);

                response.setLastBets(convertDBtoObjectLastBet(lastBets));
                response.setStatus(Constants.SUCCESS);
                rouletteDao.closeRoulette(roulette);
                rouletteDao.closeLastBets(lastBets);
            } else {
                response.setStatus(Constants.ERROR);
            }

        } catch (Exception ex) {
            response.setStatus(Constants.ERROR);
            LOGGER.severe(ex.getMessage());
        }

        return response;
    }

    public List<LastBet> convertDBtoObjectLastBet(List<BetsRoulette> listDB) {
        try {
            List<LastBet> result = new ArrayList<>();
            for (BetsRoulette data : listDB) {
                LastBet lastBet = new LastBet();
                lastBet.setBet(data.getRouletteOption());
                lastBet.setBetValue(data.getValueBet());
                lastBet.setUser(data.getUser());
                result.add(lastBet);
            }

            return result;
        } catch (Exception ex) {
            throw ex;
        }
    }

    public GetRoulettesResponse getRoulettesList() {
        GetRoulettesResponse response = new GetRoulettesResponse();
        try {
            List<Roulettes> rouletteList = rouletteDao.getRoulettes();
            response.setRouletteList(convertDBtoObjectRoulettes(rouletteList));
            response.setStatus(Constants.SUCCESS);

        } catch (Exception ex) {
            response.setStatus(Constants.ERROR);
            LOGGER.severe(ex.getMessage());
        }

        return response;

    }

    public List<Roulette> convertDBtoObjectRoulettes(List<Roulettes> listDB) {
        try {
            List<Roulette> result = new ArrayList<>();
            for (Roulettes data : listDB) {
                Roulette roulette = new Roulette();
                roulette.setRoulette(data.getIdRoulette());
                roulette.setStatus(getStatusByInt(data.getStatus()));
                result.add(roulette);
            }

            return result;
        } catch (Exception ex) {
            throw ex;
        }
    }

    public String getStatusByInt(int status) {
        try {
            switch (status) {
                case 1:
                    return EEstado.ABIERTO.textValue();
                case 0:
                    return EEstado.CERRADO.textValue();
                default:
                    return "";
            }
        } catch (Exception ex) {
            throw ex;
        }

    }

}
