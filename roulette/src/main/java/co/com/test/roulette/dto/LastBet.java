package co.com.test.roulette.dto;

/**
 *
 * @author Jesus Eduardo Martinez Guerra
 */
public class LastBet {

    private String bet;
    private int betValue;
    private String user;

    public String getBet() {
        return bet;
    }

    public void setBet(String bet) {
        this.bet = bet;
    }

    public int getBetValue() {
        return betValue;
    }

    public void setBetValue(int betValue) {
        this.betValue = betValue;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

}
