package co.com.test.roulette.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * @author Jesus Eduardo Martinez Guerra
 */
@Entity
@Table(name = "roulettes")
@NamedQueries({
    @NamedQuery(name = "roulettes.findRoulette",
      query = "SELECT r FROM Roulettes r WHERE r.idRoulette = :idRoulette")
    ,
  @NamedQuery(name = "roulettes.findRouletteByIdAndStatus",
      query = "SELECT r FROM Roulettes r "
      + "WHERE r.idRoulette = :idRoulette and r.status = :status")
    ,
  @NamedQuery(name = "roulettes.findRoulettes",
      query = "SELECT r FROM Roulettes r "),}
)
public class Roulettes implements Serializable {

    private static final long serialVersionUID = 4743380755980307299L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_roulette")
    private Integer idRoulette;

    @Column(name = "status")
    private Integer status;

    public Roulettes() {
        this.status = 0;
    }

    public Integer getIdRoulette() {
        return idRoulette;
    }

    public void setIdRoulette(Integer idRoulette) {
        this.idRoulette = idRoulette;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

}
