package co.com.test.roulette.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * @author Jesus Eduardo Martinez Guerra
 */
@Entity
@Table(name = "bets_roulette")
@NamedQueries(
  @NamedQuery(name = "betsRoulette.findBetsByRouletteAndStatus",
    query = "SELECT br FROM BetsRoulette br "
    + "WHERE br.idRoulette = :idRoulette AND br.status = :status")
)
public class BetsRoulette implements Serializable {

    private static final long serialVersionUID = -3655566864064807215L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_bet")
    private Integer idBet;

    @Column(name = "id_roulette")
    private Integer idRoulette;

    @Column(name = "roulette_option")
    private String rouletteOption;

    @Column(name = "value_bet")
    private int valueBet;

    @Column(name = "user_bet")
    private String user;

    @Column(name = "status")
    private int status;

    public Integer getIdBet() {
        return idBet;
    }

    public void setIdBet(Integer idBet) {
        this.idBet = idBet;
    }

    public Integer getIdRoulette() {
        return idRoulette;
    }

    public void setIdRoulette(Integer idRoulette) {
        this.idRoulette = idRoulette;
    }

    public String getRouletteOption() {
        return rouletteOption;
    }

    public void setRouletteOption(String rouletteOption) {
        this.rouletteOption = rouletteOption;
    }

    public int getValueBet() {
        return valueBet;
    }

    public void setValueBet(int valueBet) {
        this.valueBet = valueBet;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

}
