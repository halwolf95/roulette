package co.com.test.roulette.services.dao;

import co.com.test.roulette.entity.BetsRoulette;
import co.com.test.roulette.entity.Roulettes;
import co.com.test.roulette.enums.EEstado;
import co.com.test.roulette.util.Constants;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Jesus Eduardo Martinez Guerra
 */
@Stateless
public class RouletteDao {

    @PersistenceContext(unitName = "PU_TEST")
    private EntityManager em;

    public Integer createRoulette() {
        try {
            Roulettes roulette = new Roulettes();
            em.persist(roulette);
            em.flush();

            return roulette.getIdRoulette();
        } catch (Exception ex) {
            throw ex;
        }
    }

    public Roulettes findRoulette(int idRoulette) throws Exception {
        try {
            Query query = em
              .createNamedQuery("roulettes.findRoulette", Roulettes.class)
              .setParameter("idRoulette", idRoulette);
            List<Roulettes> result = query.getResultList();
            if (result.isEmpty()) {
                throw new Exception(Constants.ERROR_QUERY_WITHOUT_RESULTS);
            } else {
                return result.get(0);
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    public void openRoulette(Roulettes roulette) {
        try {
            roulette.setStatus(EEstado.ABIERTO.value());
            em.merge(roulette);
        } catch (Exception ex) {
            throw ex;
        }
    }

    public void closeRoulette(Roulettes roulette) {
        try {
            roulette.setStatus(EEstado.CERRADO.value());
            em.merge(roulette);
        } catch (Exception ex) {
            throw ex;
        }
    }

    public void closeLastBets(List<BetsRoulette> listLastBets) {
        try {
            for (BetsRoulette lastBet : listLastBets) {
                lastBet.setStatus(EEstado.CERRADO.value());
                em.merge(lastBet);
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    public Roulettes findRouletteByIdAndStatud(int idRoulette) throws Exception {
        try {
            Query query = em
              .createNamedQuery("roulettes.findRouletteByIdAndStatus", Roulettes.class)
              .setParameter("idRoulette", idRoulette)
              .setParameter("status", EEstado.ABIERTO.value());
            List<Roulettes> result = query.getResultList();
            if (result.isEmpty()) {
                return null;
            } else {
                return result.get(0);
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    public void createBetRoulette(BetsRoulette bet) {
        try {
            em.persist(bet);
        } catch (Exception ex) {
            throw ex;
        }
    }

    public List<BetsRoulette> findBetsByRouletteAndStatus(int idRoulette)
      throws Exception {
        try {
            Query query = em
              .createNamedQuery("betsRoulette.findBetsByRouletteAndStatus",
                BetsRoulette.class)
              .setParameter("idRoulette", idRoulette)
              .setParameter("status", EEstado.ABIERTO.value());
            List<BetsRoulette> result = query.getResultList();
            return result;
        } catch (Exception ex) {
            throw ex;
        }
    }

    public List<Roulettes> getRoulettes() {
        try {
            Query query = em
              .createNamedQuery("roulettes.findRoulettes", Roulettes.class);
            List<Roulettes> result = query.getResultList();
            return result;
        } catch (Exception ex) {
            throw ex;
        }
    }

}
