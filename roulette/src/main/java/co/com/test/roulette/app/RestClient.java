package co.com.test.roulette.app;

import co.com.test.roulette.request.BetRouletteRequest;
import co.com.test.roulette.response.CloseRouletteResponse;
import co.com.test.roulette.response.GetRoulettesResponse;
import co.com.test.roulette.response.RouletteResponse;
import co.com.test.roulette.services.ejb.RouletteServices;
import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Produces;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;

/**
 * REST Web Service
 *
 * @author Jesus Eduardo Martinez Guerra
 */
@Path("/")
public class RestClient {

    @EJB
    RouletteServices services;

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Path("createRoulette")
    public RouletteResponse createRoulette() {
        return services.createRoulette();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Path("openRoulette/{value}")
    public RouletteResponse openRoulette(@PathParam("value") String value) {
        return services.openRoulette(value);
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Path("betOnRoulette")
    public RouletteResponse betOnRoulette(@HeaderParam("user") String user,
      BetRouletteRequest request) {
        return services.betOnRoulette(user, request);
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Path("closeRoulette/{value}")
    public CloseRouletteResponse closeRoulette(@PathParam("value") String value) {
        return services.closeRoulette(value);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("getRoulettesList")
    public GetRoulettesResponse getRoulettesList() {
        return services.getRoulettesList();
    }

}
