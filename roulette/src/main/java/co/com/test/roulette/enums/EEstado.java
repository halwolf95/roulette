/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.test.roulette.enums;

/**
 *
 * @author martinezjed
 */
public enum EEstado {

    CERRADO {
        @Override
        public String textValue() {
            return "Cerrado";
        }

        @Override
        public int value() {
            return 0;
        }

    },
    ABIERTO {
        @Override
        public String textValue() {
            return "Abierto";
        }

        @Override
        public int value() {
            return 1;
        }

    };

    public abstract String textValue();

    public abstract int value();

}
