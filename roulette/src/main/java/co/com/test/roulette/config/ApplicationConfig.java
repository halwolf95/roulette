package co.com.test.roulette.config;

import java.util.Set;
import javax.ws.rs.core.Application;

/**
 * @author Jesus Eduardo Martinez Guerra
 */
@javax.ws.rs.ApplicationPath("/ws")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }

    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(co.com.test.roulette.app.RestClient.class);
    }

}
