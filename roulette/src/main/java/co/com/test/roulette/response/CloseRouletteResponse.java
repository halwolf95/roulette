package co.com.test.roulette.response;

import co.com.test.roulette.dto.LastBet;
import java.util.List;

/**
 *
 * @author Jesus Eduardo Martinez Guerra
 */
public class CloseRouletteResponse extends RouletteResponse {

    private List<LastBet> lastBets;

    public List<LastBet> getLastBets() {
        return lastBets;
    }

    public void setLastBets(List<LastBet> lastBets) {
        this.lastBets = lastBets;
    }

}
